# [LINFO2252] - Smart Messaging System

"Smart" Java Application which allows modifications depending on varying contexts in runtime.

## Authors

- Guillaume Jadin
- Thomas Robert

## Implementation

### Use of Design Patterns

Runners to interact with the application:
    
- **Runner**: Abstract class which implements an Abstract Factory Pattern of two classes *Command* and *Action*.
- **Command**: Abstract class which extends Runner and implements a Factory Method. It is used by the user to browse between all the available commands and actions that he/she can do.
- **Action**: Abstract class which extends Runner and implements a Factory Method. It is used by a command (which extends the abstract class *Command*) to perform specific actions.
- **Modularity**: Class implemented as a Singleton which saves and interacts with all the instances of *Action* and *Command* implemented classes. It is used to activate/deactivate features in the application at runtime.
- **CLI**: Used to print some useful structure for the Command-Line Interface.

Users implementation:

- **GenUserID**: Class implemented as a Singleton. Used to generate a unique ID for each new user.

### Maintenance & Evolution

- There is a command package, containing all the commands on the application. Same for action.
- Each feature possesses packages of its name in different places => easy to read and see the separation between them.
- Just has to extend/implement an abstract class/interface which set the structure or the methods that never change (or not so much) between classes that inherit. And then override some basic methods.
- The classes used to show the Command-Line User Interface use Abstract Factory and Factory Method Design Patterns which helps a lot in the reusability and the maintainability of the project.

For example, if a developer needs to add a new feature, he must do the following work:

1. Create the feature using the heritage between classes.
2. Create a new Command/Action with which the user will interact.
3. Extend the Command/Action with the corresponding abstract class.
4. Implements two methods:

   - **For all**: *String info()* which returns the description of the Command/Action.
   - **For the Command instances**: *void feedAll()* which adds a Command or an Action to the current Command.
   - **For the Action instances**: *void run(User currentUser, String[] cmd)* which do something with the feature implemented before.

## Explanation on modularity

To achieve modularity in the application, we must first enter the command modularity from the main context and then choose to *activate* or *deactivate* in the following format:

```
activate feature_1 feature_2 feature_3
deactivate feature_1 feature_2 feature_3
```

This Action will run over the feature names in the command and for each, it will get the reference to it via the *Modularity* class and the *optionName*. Once it has the reference, it launches the method *activate()/deactivate()* on it. What this method does is that it sets the state to *true/false* so that when the *run* method from *Command* class is run, we check and launch or not this command.
