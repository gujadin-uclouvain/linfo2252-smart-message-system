package be.uclouvain.linfo2252.sme10.runners;

import be.uclouvain.linfo2252.sme10.conversations.Conversation;
import be.uclouvain.linfo2252.sme10.runners.commands.Command;
import be.uclouvain.linfo2252.sme10.runners.commands.ExitCommand;
import be.uclouvain.linfo2252.sme10.users.User;

import java.util.Arrays;

public class State {
    private Command currCommand;
    private User currUser;
    private Conversation currConv;
    private String cmdRunner = "";
    private String[] cmdArgs = new String[0];

    public Command getCurrCommand() { return currCommand; }
    public void setCurrCommand(Command currCommand) { this.currCommand = currCommand; }
    public void resetCurrCommand() { this.currCommand = new ExitCommand(); }

    public User getCurrUser() { return currUser; }
    public void setCurrUser(User currUser) { this.currUser = currUser; }
    public void resetCurrUser() { this.currUser = null; }

    public Conversation getCurrConv() { return currConv; }
    public void setCurrConv(Conversation currConv) { this.currConv = currConv; }
    public void resetCurrConv() { this.currConv = null; }

    public String getCMDRunner() { return cmdRunner; }
    public String[] getCMDArgs() { return cmdArgs; }
    public void setCMD(String[] cmd) {
        this.cmdRunner = cmd[0];
        this.cmdArgs = cmd.length > 1 ? Arrays.stream(cmd).skip(1).toList().toArray(new String[cmd.length-1])
                                      : new String[0];
    }
}
