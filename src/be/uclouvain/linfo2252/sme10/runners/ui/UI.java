package be.uclouvain.linfo2252.sme10.runners.ui;

import be.uclouvain.linfo2252.sme10.runners.Runner;
import be.uclouvain.linfo2252.sme10.runners.State;
import be.uclouvain.linfo2252.sme10.runners.tools.Pair;
import be.uclouvain.linfo2252.sme10.runners.ui.cli.CLI;
import be.uclouvain.linfo2252.sme10.runners.ui.gui.GUI;

import java.util.ArrayList;

public enum UI {
    INSTANCE;
    public final CLI cli = new CLI();
    private final GUI gui = new GUI();

    /* Commands CLI/GUI Interface */
    public void setup(State currState) {
        ArrayList<Pair<String, Runner>> cmdCollection = currState.getCurrCommand().getMethods();
        gui.showMethods(currState, cmdCollection);
        cli.showMethods(currState, cmdCollection);
    }

    public void display(String str) {
        cli.display(str);
        gui.display(str, str);
    }

    public void displayHelp(Runner currentRunner, ArrayList<Pair<String, Runner>> cmdCollection) {
        cli.displayHelp(currentRunner, cmdCollection);
    }

    public void displayOptionalFeatures() {
        cli.displayOptionalFeatures();
    }

    public void changeBackground() {
        this.gui.switchBackground();
    }

    /* SUCCESS - Action & Command  */
    public void success(String info) {
        cli.success(info);
    }

    public void switchColorFeature(String feature, boolean newState) {
        cli.switchColorFeature(feature, newState);
    }

    /* ERRORS - Action & Command  */
    public void fail(String info) {
        cli.fail(info);
    }

    public void notExist(String cmd) {
        cli.notExist(cmd);
    }
}
