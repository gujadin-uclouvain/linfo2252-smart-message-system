package be.uclouvain.linfo2252.sme10.runners.ui.gui;

import be.uclouvain.linfo2252.sme10.runners.Runner;
import be.uclouvain.linfo2252.sme10.runners.State;
import be.uclouvain.linfo2252.sme10.runners.actions.Action;
import be.uclouvain.linfo2252.sme10.runners.commands.ExitCommand;
import be.uclouvain.linfo2252.sme10.runners.tools.Pair;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.*;

public class GUI {
    private final JFrame window;
    private final HashMap<String, JPanel> panels;
    private Color colorPanel;

    public GUI() {
        this.window = new JFrame("Smart Messaging System");
        this.panels = new HashMap<>();
        this.colorPanel = Color.WHITE;
        this.window.setAlwaysOnTop(true);

        // Stop the program when window is closed
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setSize(700, 500);
        window.setVisible(true); // show the window
    }

    private JPanel createPanel(String panelName) {
        if (this.panels.containsKey(panelName)) {
            this.window.remove(this.panels.get(panelName));
        }
        JPanel panel = new JPanel();
        this.window.add(panel);
        this.panels.put(panelName, panel);
        panel.setBackground(this.colorPanel);
        return panel;
    }

    private void createButton(String name, JPanel currPanel, ActionListener listener) {
        JButton button = new JButton(name);
        button.addActionListener(listener);
        currPanel.add(button);
    }

    private JTextField createTextField(JPanel currPanel) {
        JTextField textField = new JTextField(16);
        currPanel.add(textField);
        return textField;
    }

    private void addTextArea(String text, JPanel currPanel) {
        currPanel.add(new JTextArea(text));
    }

    public void switchBackground() {
        this.colorPanel = this.colorPanel == Color.WHITE ? Color.DARK_GRAY : Color.WHITE;
        refreshCurrentBackground();
    }

    private void refreshCurrentBackground() {
        this.panels.forEach((idName, panel) -> {
            panel.setBackground(this.colorPanel);
        });
        updateGUI();
    }

    public void updateGUI() {
        /*
         * These lines update the UI
         * Repaint holds for all graphical changes (like color changes)
         * Revalidate holds for UI change (like button add/remove)
         */
        window.repaint();
        window.revalidate();
    }

    /* ----------------------------------------------------------------------------------------- */

    /* Commands CLI/GUI Interface */
    public void display(String str, String panelName) {
        this.addTextArea(str, this.createPanel(panelName));
    }

    private String[] getArgs(String runner, JTextField textField) {
        ArrayList<String> arrayCMD = new ArrayList<>();
        arrayCMD.add(runner);

        String[] splitArgs = textField.getText().strip().split(" ");
        if (splitArgs.length > 0 && !splitArgs[0].isEmpty()) {
            Collections.addAll(arrayCMD, splitArgs);
        }

        return arrayCMD.toArray(new String[splitArgs.length]);
    }

    public void showMethods(State currState, ArrayList<Pair<String, Runner>> cmdCollection) {
        JPanel methodsPanel = this.createPanel("methods");

        JTextField argsTextField = this.createTextField(methodsPanel);
        cmdCollection.forEach(cmdRunnerPair -> {
            if (!(cmdRunnerPair.getSecond() instanceof ExitCommand)) { // Prevent bug with blocking System.in state
                this.createButton(cmdRunnerPair.getFirst(), methodsPanel, event -> {
                    currState.setCMD(this.getArgs(cmdRunnerPair.getFirst(), argsTextField));
                    currState.setCurrCommand(currState.getCurrCommand().run(currState));
                });
            }
        });
        this.updateGUI();
    }
}
