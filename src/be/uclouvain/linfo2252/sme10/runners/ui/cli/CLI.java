package be.uclouvain.linfo2252.sme10.runners.ui.cli;

import be.uclouvain.linfo2252.sme10.runners.Runner;
import be.uclouvain.linfo2252.sme10.runners.State;
import be.uclouvain.linfo2252.sme10.runners.modularities.Modularity;
import be.uclouvain.linfo2252.sme10.runners.tools.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CLI {
    private final int size = 100;

    /* Commands CLI Interface */
    public void showMethods(State currState, ArrayList<Pair<String, Runner>> cmdCollection) {
        this.displayAllNextCMDs(cmdCollection);
        currState.setCMD(CLI.getArgs());
        currState.setCurrCommand(currState.getCurrCommand().run(currState));
    }

    public void display(String str) {
        System.out.println(str);
    }

    public void centerDisplay(String str) {
        String format = " ".repeat((size - str.length()) / 2);
        System.out.println(format + str + format);
    }

    public void displayAllNextCMDs(ArrayList<Pair<String, Runner>> cmdCollection) {
        this.sep();
        StringBuilder printer = new StringBuilder();
        printer.append('[');
        cmdCollection.forEach(cmdRunnerPair -> {
            printer.append(
                    ConsoleColors.colorize(ConsoleColors.BLUE, cmdRunnerPair.getFirst())
            ).append(", ");
        });
        printer.replace(printer.length()-2, printer.length()-1, "]");
        System.out.println(printer);
        System.out.println("Enter a command (see help): ");
    }

    public void displayHelp(Runner currentRunner, ArrayList<Pair<String, Runner>> cmdCollection) {
        this.sepAlt();
        StringBuilder printer = new StringBuilder();
        printer.append(ConsoleColors.colorize(ConsoleColors.PURPLE, currentRunner.info())).append("\n\n");
        printer.append("Possible commands:\n");
        String format = "%-30.30s";
        cmdCollection.stream().filter(stringRunnerPair -> stringRunnerPair.getSecond().isActivated()).forEach(cmdRunnerPair -> {
            printer.append("  ")
                   .append(format.formatted(
                           ConsoleColors.colorize(ConsoleColors.BLUE_BOLD, cmdRunnerPair.getFirst()))
                   )
                   .append(
                           ConsoleColors.colorize(ConsoleColors.CYAN, cmdRunnerPair.getSecond().info())
                   )
                   .append("\n");
        });
        printer.delete(printer.length()-1, printer.length());
        System.out.println(printer);
        this.sepAlt();
    }

    public void displayOptionalFeatures() {
        this.sepAlt();
        Modularity.INSTANCE.getAll().forEach(entry -> {
            String ANSI = entry.getValue().isActivated() ? ConsoleColors.GREEN_BOLD : ConsoleColors.RED_BOLD;
            String s = "%-70.70s".formatted(ConsoleColors.colorize(ANSI, entry.getKey()))
                     + "%32.32s".formatted(ConsoleColors.colorize(ANSI, String.valueOf(entry.getValue().isActivated())));
            System.out.println(s);
        });
        this.sepAlt();
    }

    /* Helpers & Tools */
    public void sep() {
        System.out.println("-".repeat(size));
    }
    public void sepAlt() {
        System.out.println("=".repeat(size));
    }


    /* SUCCESS - Action & Command  */
    public void success(String info) {
        System.out.print(ConsoleColors.colorize(ConsoleColors.GREEN_BOLD, "[SUCCESS] %s".formatted(info)));
    }

    public void switchColorFeature(String feature, boolean newState) {
        String ANSI = newState ? ConsoleColors.GREEN_BOLD : ConsoleColors.RED_BOLD;
        this.success(ConsoleColors.colorize(ANSI, "%s -> %s\n".formatted(feature, newState)));
    }

    /* ERRORS - Action & Command  */
    public void fail(String info) {
        System.out.print(ConsoleColors.colorize(ConsoleColors.RED_BOLD, "[FAILED] %s".formatted(info)));
    }

    public void notExist(String cmd) {
        System.out.print(ConsoleColors.colorize(ConsoleColors.YELLOW_BOLD, "'%s' is not a valid command.\n".formatted(cmd)));
    }


    /* Static Methods */
    /**
     * Asks the user to enter a String
     */
    public static String[] getArgs() { return Input.getInputs().split(" "); }
}
