package be.uclouvain.linfo2252.sme10.runners.actions.profiles;

import be.uclouvain.linfo2252.sme10.runners.State;
import be.uclouvain.linfo2252.sme10.runners.actions.Action;

public class SetNamesProfileAction extends Action {
    @Override
    public String info() {
        return "Set the firstname and lastname. Use: \"$setnames firstname lastname\"";
    }

    @Override
    public void run(State currState) {
        if (this.equalNbrArgs(2, currState.getCMDArgs().length)) return;
        currState.getCurrUser().setNames(currState.getCMDArgs()[0], currState.getCMDArgs()[1]);
        this.ui.success("Firstname updated to %s\n".formatted(currState.getCMDArgs()[0]));
        this.ui.success("Lastname updated to %s\n".formatted(currState.getCMDArgs()[1]));
    }
}
