package be.uclouvain.linfo2252.sme10.runners.actions.profiles;

import be.uclouvain.linfo2252.sme10.runners.State;
import be.uclouvain.linfo2252.sme10.runners.actions.Action;

public class SetUsernameProfileAction extends Action {
    @Override
    public String info() {
        return "Set the username to another one. Use: \"$setusername username\"";
    }

    @Override
    public void run(State currState) {
        if (this.equalNbrArgs(1, currState.getCMDArgs().length)) return;
        currState.getCurrUser().setUsername(currState.getCMDArgs()[0]);
        this.ui.success("Username updated to %s\n".formatted(currState.getCMDArgs()[0]));
    }
}
