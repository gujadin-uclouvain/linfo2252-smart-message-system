package be.uclouvain.linfo2252.sme10.runners.actions.profiles;

import be.uclouvain.linfo2252.sme10.runners.State;
import be.uclouvain.linfo2252.sme10.runners.actions.Action;
import be.uclouvain.linfo2252.sme10.users.User;

public class ShowProfileAction extends Action {
    @Override
    public String info() {
        return "Show the profile of the user";
    }

    @Override
    public void run(State currState) {
        User user = currState.getCurrUser();
        System.out.println("Username of the current user: " + user.getUsername());
        System.out.println("Id of the current user: " + user.getId());
        if (user.getFirstName() != null) {
            System.out.println("Firstname of the current user: " + user.getFirstName());
        } else {
            System.out.println("No firstname currently set");
        }
        if (user.getLastName() != null) {
            System.out.println("Lastname of the current user: " + user.getLastName());
        } else {
            System.out.println("No lastname currently set");
        }
    }
}
