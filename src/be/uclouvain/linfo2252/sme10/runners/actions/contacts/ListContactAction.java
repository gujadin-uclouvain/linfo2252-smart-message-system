package be.uclouvain.linfo2252.sme10.runners.actions.contacts;

import be.uclouvain.linfo2252.sme10.runners.State;
import be.uclouvain.linfo2252.sme10.runners.actions.Action;

public class ListContactAction extends Action {
    @Override
    public String info() {
        return "List all the contact of a user";
    }

    @Override
    public void run(State currState) {
        this.ui.display(currState.getCurrUser().getContacts().values().toString());
    }
}
