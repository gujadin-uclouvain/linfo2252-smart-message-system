package be.uclouvain.linfo2252.sme10.runners.actions.contacts;

import be.uclouvain.linfo2252.sme10.runners.State;
import be.uclouvain.linfo2252.sme10.runners.actions.Action;
import be.uclouvain.linfo2252.sme10.runners.databases.UsersDB;
import be.uclouvain.linfo2252.sme10.users.User;

public class DelContactAction extends Action {
    @Override
    public String info() {
        return "Delete a user by its ID from the contact list";
    }

    private void removeContact(User currUser, int userId) {
        User oldFriend = currUser.getContacts().get(userId);
        currUser.removeContact(oldFriend);
        this.ui.success("You successfully removed %s to your contact list\n".formatted(oldFriend.getUsername()));
    }

    @Override
    public void run(State currState) {
        if (this.biggerNbrArgs(1, currState.getCMDArgs().length)) return;
        for (String selectedUser : currState.getCMDArgs()) {
            try {
                removeContact(currState.getCurrUser(), Integer.parseInt(selectedUser));
            } catch (Exception e) {
                this.ui.fail("The user with the ID '%s' is not in your contact list\n".formatted(selectedUser));
            }
        }
    }
}