package be.uclouvain.linfo2252.sme10.runners.actions.contacts;

import be.uclouvain.linfo2252.sme10.runners.State;
import be.uclouvain.linfo2252.sme10.runners.actions.Action;
import be.uclouvain.linfo2252.sme10.runners.databases.UsersDB;
import be.uclouvain.linfo2252.sme10.users.User;

public class AddContactAction extends Action {
    @Override
    public String info() {
        return "Add a user to the contact list";
    }

    private void addContact(User currUser, int userId) {
        User newFriend = UsersDB.INSTANCE.getUser(userId);
        if (currUser.equals(newFriend)) {
            this.ui.fail("You cannot add yourself in your contact list\n");
            return;
        };
        if (!currUser.addContact(newFriend)) {
            this.ui.fail("The user `%s` is already in your contact list\n".formatted(newFriend.getUsername()));
            return;
        };
        this.ui.success("You successfully added %s to your contact list\n".formatted(newFriend.getUsername()));
    }

    @Override
    public void run(State currState) {
        if (this.biggerNbrArgs(1, currState.getCMDArgs().length)) return;
        for (String selectedUser : currState.getCMDArgs()) {
            try {
                addContact(currState.getCurrUser(), Integer.parseInt(selectedUser));
            } catch (Exception e) {
                this.ui.fail("The user with the ID '%s' doesn't exist\n".formatted(selectedUser));
            }
        }
    }
}
