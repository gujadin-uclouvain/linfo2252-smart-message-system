package be.uclouvain.linfo2252.sme10.runners.actions.notifications;

import be.uclouvain.linfo2252.sme10.notifications.Notification;
import be.uclouvain.linfo2252.sme10.runners.State;

public class VibrateNotificationAction extends NotificationAction {
    @Override
    public String info() {
        return "Notification mode without sound and with vibration.";
    }

    @Override
    public void run(State currState) {
        this.manageUserNotification(currState.getCurrUser(), Notification.VIBRATE.getNotifiedSimulation());
    }
}
