package be.uclouvain.linfo2252.sme10.runners.actions.notifications;

import be.uclouvain.linfo2252.sme10.runners.actions.Action;
import be.uclouvain.linfo2252.sme10.users.User;

public abstract class NotificationAction extends Action {
    protected void manageUserNotification(User user, String notification) {
        if (user.hasNotification()) {
            this.ui.display(notification);
            user.readNotification();
        }
    }
}
