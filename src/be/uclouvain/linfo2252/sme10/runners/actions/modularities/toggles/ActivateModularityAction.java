package be.uclouvain.linfo2252.sme10.runners.actions.modularities.toggles;

import be.uclouvain.linfo2252.sme10.runners.Runner;

public class ActivateModularityAction extends ToggleModularityAction {
    @Override
    public String info() {
        return "Activate some features";
    }

    @Override
    protected void toggleModularity(Runner feature) {
        feature.activate();
    }
}
