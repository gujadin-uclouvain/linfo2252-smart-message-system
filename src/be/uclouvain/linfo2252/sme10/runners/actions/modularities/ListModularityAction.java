package be.uclouvain.linfo2252.sme10.runners.actions.modularities;

import be.uclouvain.linfo2252.sme10.runners.State;
import be.uclouvain.linfo2252.sme10.runners.actions.Action;

public class ListModularityAction extends Action {
    @Override
    public String info() {
        return "List all the features which can be activated / deactivated";
    }

    @Override
    public void run(State currState) {
        this.ui.displayOptionalFeatures();
    }
}
