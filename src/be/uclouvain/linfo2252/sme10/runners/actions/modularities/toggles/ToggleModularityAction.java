package be.uclouvain.linfo2252.sme10.runners.actions.modularities.toggles;

import be.uclouvain.linfo2252.sme10.runners.Runner;
import be.uclouvain.linfo2252.sme10.runners.State;
import be.uclouvain.linfo2252.sme10.runners.actions.Action;
import be.uclouvain.linfo2252.sme10.runners.modularities.Modularity;

public abstract class ToggleModularityAction extends Action {
    /** Activate/Deactivate the Option feature */
    protected abstract void toggleModularity(Runner feature);

    @Override
    public void run(State currState) {
        for (int i = 0; i < currState.getCMDArgs().length; i++) {
            String optionName = currState.getCMDArgs()[i];
            Runner featureToDisable = Modularity.INSTANCE.getOne(optionName);
            if (featureToDisable != null) {
                this.toggleModularity(featureToDisable);
                this.ui.switchColorFeature(currState.getCMDArgs()[i], featureToDisable.isActivated());
            } else this.ui.notExist(optionName);
        }
    }
}
