package be.uclouvain.linfo2252.sme10.runners.actions.conversations;

import be.uclouvain.linfo2252.sme10.messages.Message;
import be.uclouvain.linfo2252.sme10.runners.State;
import be.uclouvain.linfo2252.sme10.runners.actions.Action;

import java.util.Iterator;

public class ShowHistoryConversationAction extends Action {
    @Override
    public String info() {
        return "List all the messages in a conversation";
    }

    @Override
    public void run(State currState) {
        String history = "";
        if (!currState.getCurrConv().isEmpty()) {
            StringBuilder flux = new StringBuilder();
            flux.append("=".repeat(100)).append("\n");
            for (Iterator<Message> iter = currState.getCurrConv().iterateAllMessages(); iter.hasNext(); ) {
                Message msg = iter.next();
                flux.append(msg.toString()).append("\n");
                if (iter.hasNext()) flux.append("-".repeat(80)).append("\n");
            }
            flux.append("=".repeat(100));
            history = String.valueOf(flux);
        }
        System.out.println(history);
    }
}
