package be.uclouvain.linfo2252.sme10.runners.actions.conversations.messages;

import be.uclouvain.linfo2252.sme10.messages.Message;
import be.uclouvain.linfo2252.sme10.messages.MessagePicture;
import be.uclouvain.linfo2252.sme10.runners.State;
import be.uclouvain.linfo2252.sme10.runners.actions.Action;

public class SendPictureAction extends Action {
    @Override
    public String info() {
        return "Send a picture to a user in a conversation";
    }

    @Override
    public void run(State currState) {
        Message msg = new MessagePicture("\n===============\n" +
                                                    "====  0 0  ====\n" +
                                                    "=====( v )=====\n" +
                                                    "===============");
        currState.getCurrUser().send(msg, currState.getCurrConv());
        this.ui.success("Your picture has been sent with success\n");
    }
}
