package be.uclouvain.linfo2252.sme10.runners.actions.conversations.messages;

import be.uclouvain.linfo2252.sme10.messages.Message;
import be.uclouvain.linfo2252.sme10.messages.MessageWritten;
import be.uclouvain.linfo2252.sme10.runners.State;
import be.uclouvain.linfo2252.sme10.runners.actions.Action;

import java.util.Arrays;

public class SendMessageAction extends Action {
    @Override
    public String info() {
        return "Send a message to a user in a conversation";
    }

    @Override
    public void run(State currState) {
        if (this.biggerNbrArgs(1, currState.getCMDArgs().length)) return;
        Message msg = new MessageWritten(currState.getCMDArgs());
        currState.getCurrUser().send(msg, currState.getCurrConv());
        this.ui.success("Your message has been sent with success\n");
    }
}
