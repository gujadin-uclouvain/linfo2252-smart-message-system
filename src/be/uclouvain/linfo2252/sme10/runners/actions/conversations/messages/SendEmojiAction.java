package be.uclouvain.linfo2252.sme10.runners.actions.conversations.messages;

import be.uclouvain.linfo2252.sme10.messages.Emoji;
import be.uclouvain.linfo2252.sme10.messages.MessageWritten;
import be.uclouvain.linfo2252.sme10.runners.State;
import be.uclouvain.linfo2252.sme10.runners.actions.Action;

import java.util.Arrays;

public class SendEmojiAction extends Action {
    @Override
    public String info() {
        return "Send an emoji to a user in a conversation.";
    }

    @Override
    public void run(State currState) {
        if (this.equalNbrArgs(1, currState.getCMDArgs().length)) return;
        try {
            MessageWritten msg = new MessageWritten(Emoji.valueOf(currState.getCMDArgs()[0]).get());
            currState.getCurrUser().send(msg, currState.getCurrConv());
            this.ui.success("Your emoji has been sent with success\n");
        } catch (IllegalArgumentException e) {
            this.ui.fail("Your emoji is unknown\n");
            this.ui.display("Please try one of the following emoji: %s".formatted(Arrays.toString(Emoji.values())));
        }
    }
}
