package be.uclouvain.linfo2252.sme10.runners.actions.conversations;

import be.uclouvain.linfo2252.sme10.conversations.Conversation;
import be.uclouvain.linfo2252.sme10.runners.State;
import be.uclouvain.linfo2252.sme10.runners.actions.Action;
import be.uclouvain.linfo2252.sme10.users.User;

import java.util.LinkedList;

public class ListConversationAction extends Action {
    @Override
    public String info() {
        return "List all the conversation of a user";
    }

    @Override
    public void run(State currState) {
        System.out.println("The list of conversation:");
        LinkedList<Conversation> conv = currState.getCurrUser().getConversations();
        for (int i=0; i<conv.size(); i++) {
            System.out.print(i);
            System.out.print(".");
            for (User nextUser : conv.get(i).getMembers().values()) {
                if (!nextUser.equals(currState.getCurrUser())) {
                    System.out.print(" ");
                    System.out.print(nextUser.getUsername());
                }
            }
            System.out.println(".");
        }
    }
}
