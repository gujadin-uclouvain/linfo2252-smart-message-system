package be.uclouvain.linfo2252.sme10.runners.actions;

import be.uclouvain.linfo2252.sme10.runners.Runner;
import be.uclouvain.linfo2252.sme10.runners.State;

import java.util.Scanner;

public abstract class Action extends Runner {
    public abstract void run(State currState);
}
