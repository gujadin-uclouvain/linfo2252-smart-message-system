package be.uclouvain.linfo2252.sme10.runners.actions.gui;

import be.uclouvain.linfo2252.sme10.runners.State;
import be.uclouvain.linfo2252.sme10.runners.actions.Action;

public class SwitchColorGUIAction extends Action {
    @Override
    public String info() {
        return "Switch the GUI theme between light and dark.";
    }

    @Override
    public void run(State currState) {
        this.ui.changeBackground();
    }
}
