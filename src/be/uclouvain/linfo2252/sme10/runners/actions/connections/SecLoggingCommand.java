package be.uclouvain.linfo2252.sme10.runners.actions.connections;

import be.uclouvain.linfo2252.sme10.runners.State;
import be.uclouvain.linfo2252.sme10.runners.actions.Action;
import be.uclouvain.linfo2252.sme10.runners.databases.UsersDB;
import be.uclouvain.linfo2252.sme10.users.SecureUser;

public class SecLoggingCommand extends Action {
    @Override
    public String info() {
        return "Log in a user in the application in a secure way (thanks to its ID and password) and access the app";
    }

    private void loggingUser(State currState) {
        String selectedUser = currState.getCMDArgs()[0];
        String password = currState.getCMDArgs()[1];
        int userId;
        UsersDB usersDB = UsersDB.INSTANCE;
        try {
            userId = Integer.parseInt(selectedUser);
            SecureUser user = (SecureUser) usersDB.getUser(userId);
            if (user.isCorrectPassword(password)) {
                currState.setCurrUser(usersDB.getUser(userId));
            }
        } catch (Exception ignored) {}
    }

    @Override
    public void run(State currState) {
        currState.resetCurrUser();
        if (this.equalNbrArgs(2, currState.getCMDArgs().length)) return;
        this.loggingUser(currState);
        if (currState.getCurrUser() != null) {
            this.ui.success(String.format("User %s successfully logged in\n", currState.getCurrUser().getUsername()));
        } else {
            this.ui.fail("User has not been found\n");
        }
    }
}
