package be.uclouvain.linfo2252.sme10.runners.actions.connections;

import be.uclouvain.linfo2252.sme10.runners.State;
import be.uclouvain.linfo2252.sme10.runners.actions.Action;
import be.uclouvain.linfo2252.sme10.runners.commands.Command;
import be.uclouvain.linfo2252.sme10.runners.databases.UsersDB;

public class LoggingAction extends Action {
    @Override
    public String info() {
        return "Log in a user in the application (thanks to its ID) and access the app";
    }

    private void loggingUser(State currState) {
        String selectedUser = currState.getCMDArgs()[0];
        int userId;
        UsersDB usersDB = UsersDB.INSTANCE;
        try {
            userId = Integer.parseInt(selectedUser);
            currState.setCurrUser(usersDB.getUser(userId));
        } catch (Exception ignored) {}
    }

    @Override
    public void run(State currState) {
        currState.resetCurrUser();
        if (this.equalNbrArgs(1, currState.getCMDArgs().length)) return;
        this.loggingUser(currState);
        if (currState.getCurrUser() != null) {
            this.ui.success(String.format("User %s successfully logged in\n", currState.getCurrUser().getUsername()));
        } else {
            this.ui.fail("User was not found\n");
        }
    }
}
