package be.uclouvain.linfo2252.sme10.runners.actions;

import be.uclouvain.linfo2252.sme10.runners.State;
import be.uclouvain.linfo2252.sme10.runners.commands.Command;

public final class HelpAction extends Action {
    private final Command command;
    public HelpAction(Command command) {this.command = command;}

    @Override
    public String info() {
        return "Give this help list";
    }

    @Override
    public void run(State currState) {
        command.help();
    }
}
