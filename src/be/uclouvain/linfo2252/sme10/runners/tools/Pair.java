package be.uclouvain.linfo2252.sme10.runners.tools;

public class Pair<A, B> {
    private final A a;
    private final B b;

    public Pair(A one, B two) {
        this.a = one;
        this.b = two;
    }

    public A getFirst() {
        return a;
    }

    public B getSecond() {
        return b;
    }
}
