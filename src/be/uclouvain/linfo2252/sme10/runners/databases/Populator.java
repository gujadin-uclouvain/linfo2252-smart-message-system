package be.uclouvain.linfo2252.sme10.runners.databases;

import be.uclouvain.linfo2252.sme10.users.SimpleUser;
import be.uclouvain.linfo2252.sme10.users.User;

public class Populator {
    public static void populateUsersDB() {
        User alice = new SimpleUser("Alice");
        User bob = new SimpleUser("Bob");
        User charles = new SimpleUser("Charles");
        User dora = new SimpleUser("Dora");
        User rick = new SimpleUser("Rick");

        // Database of users
        UsersDB usersDB = UsersDB.INSTANCE;
        usersDB.addUser(alice);
        usersDB.addUser(bob);
        usersDB.addUser(charles);
        usersDB.addUser(dora);
        usersDB.addUser(rick);
    }

    public static void populateContactsList(User user) {
        user.addContact(UsersDB.INSTANCE.getUser(1));
        user.addContact(UsersDB.INSTANCE.getUser(2));
        user.addContact(UsersDB.INSTANCE.getUser(3));
    }
}
