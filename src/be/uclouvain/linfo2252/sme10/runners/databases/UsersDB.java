package be.uclouvain.linfo2252.sme10.runners.databases;

import be.uclouvain.linfo2252.sme10.users.User;

import java.util.HashMap;

public enum UsersDB {
    INSTANCE;
    private final HashMap<Integer, User> users = new HashMap<>();

    public HashMap<Integer, User> getUsers() { return users; }
    public User getUser(int id) { return users.get(id); }
    public void addUser(User user) { users.put(user.getId(), user); }
}
