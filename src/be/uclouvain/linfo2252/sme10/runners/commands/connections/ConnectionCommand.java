package be.uclouvain.linfo2252.sme10.runners.commands.connections;

import be.uclouvain.linfo2252.sme10.runners.State;
import be.uclouvain.linfo2252.sme10.runners.actions.connections.LoggingAction;
import be.uclouvain.linfo2252.sme10.runners.commands.Command;

public class ConnectionCommand extends Command {
    private final Command specificHiddenPreviousCommand;
    public ConnectionCommand(Command specificPreviousCommand) {
        super();
        this.specificHiddenPreviousCommand = specificPreviousCommand;
    }

    @Override
    public String info() {
        return "Initial state of the application, where you can connect a user";
    }

    @Override
    public void feedAll() {
        feedOne("login", new LoggingAction());
        // feedOne("secLogIn", new SecLoggingAction());
    }

    @Override
    public Command filterAction(State currState, Command previousCommand) {
        return currState.getCurrUser() != null ? specificHiddenPreviousCommand : super.filterAction(currState, previousCommand);
    }
}
