package be.uclouvain.linfo2252.sme10.runners.commands.contacts;

import be.uclouvain.linfo2252.sme10.runners.actions.contacts.AddContactAction;
import be.uclouvain.linfo2252.sme10.runners.actions.contacts.DelContactAction;
import be.uclouvain.linfo2252.sme10.runners.actions.contacts.ListContactAction;
import be.uclouvain.linfo2252.sme10.runners.commands.*;

public class ContactCommand extends Command {
    @Override
    public String info() {
        return "See option on the user contact list";
    }

    @Override
    public void feedAll() {
        feedOne("list", new ListContactAction());
        feedOne("add", new AddContactAction());
        feedOne("del", new DelContactAction());
    }
}
