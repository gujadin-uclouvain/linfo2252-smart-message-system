package be.uclouvain.linfo2252.sme10.runners.commands.profiles;

import be.uclouvain.linfo2252.sme10.runners.actions.profiles.SetUsernameProfileAction;
import be.uclouvain.linfo2252.sme10.runners.actions.profiles.SetNamesProfileAction;
import be.uclouvain.linfo2252.sme10.runners.actions.profiles.ShowProfileAction;
import be.uclouvain.linfo2252.sme10.runners.commands.Command;

public class ProfileCommand extends Command {
    @Override
    public String info() {
        return "See option on the user profile";
    }

    @Override
    public void feedAll() {
        feedOne("setusername", new SetUsernameProfileAction());
        feedOne("setnames", new SetNamesProfileAction());
        feedOne("show", new ShowProfileAction());
    }
}
