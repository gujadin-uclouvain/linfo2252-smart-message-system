package be.uclouvain.linfo2252.sme10.runners.commands;

import be.uclouvain.linfo2252.sme10.runners.commands.connections.ConnectionCommand;
import be.uclouvain.linfo2252.sme10.runners.tools.Pair;
import be.uclouvain.linfo2252.sme10.runners.Runner;
import be.uclouvain.linfo2252.sme10.runners.State;
import be.uclouvain.linfo2252.sme10.runners.actions.Action;
import be.uclouvain.linfo2252.sme10.runners.actions.HelpAction;
import be.uclouvain.linfo2252.sme10.runners.modularities.Modularity;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Stream;

public abstract class Command extends Runner {
    private final LinkedHashMap<String, Runner> methods;
    private final LinkedHashMap<String, Action> hiddenActions;

    public Command() {
        this.methods = new LinkedHashMap<>();
        this.hiddenActions = new LinkedHashMap<>();
        this.feedAll();
        if (!(this instanceof ExitCommand)) { // Avoid feeding Special ExitCommand
            this.feedOne("help", new HelpAction(this));
            this.feedOne("exit", new ExitCommand());
        }
    }

    /* ------------------------------------------------------------------ */
    /* Feeders */

    /**
     * Add a Sub Command/Action in the current Command (As a main feature).
     */
    public void feedOne(String cmd, Runner runner) {
        if (!this.equals(runner)) { // Avoid feeding itself
            if (!methods.containsKey(cmd)) methods.put(cmd, runner);
        }
    }

    /**
     * Add a Sub Command/Action in the current Command (As a main feature).
     * This Runner will not be seen by the user and will be launch when the current command will be selected.
     */
    private void feedOneHidden(String cmd, Action action) {
        if (!hiddenActions.containsKey(cmd)) hiddenActions.put(cmd, action);
    }

    private void addOption(String optionName, Runner runner) {
        runner.setOptionName(optionName);
        Modularity.INSTANCE.feedOne(optionName, runner);
    }

    /**
     * Add a Sub Command/Action in the current Command (As an optional feature).
     */
    public void feedOneOption(String cmd, Runner runner, String optionName, boolean isActivatedByDefault) {
        feedOne(cmd, runner);
        addOption(optionName, runner);
        if (!isActivatedByDefault) { runner.deactivate(); }
    }

    /**
     * Add a Sub Command/Action in the current Command (As an optional feature).
     * This Runner will not be seen by the user and will be launch when the current command will be selected.
     */
    public void feedOneHiddenOption(String optionCMDName, Action action, boolean isActivatedByDefault) {
        feedOneHidden(optionCMDName, action);
        addOption(optionCMDName, action);
        if (!isActivatedByDefault) { action.deactivate(); }
    }

    /**
     * Feed dynamically the 'previous' command to get back into the previous Command.
     */
    private void feedPreviousCommandInNext(Command nextCommand) {
        if (!((nextCommand instanceof MainCommand) || (nextCommand instanceof ConnectionCommand))) {
            nextCommand.feedOne("previous", this);
        }
    }

    /**
     * --Abstract Method--
     * Add all the Commands/Actions in the current Command.
     * Use 'feedOne' et 'feedOneOption'
     */
    public abstract void feedAll();

    /* ------------------------------------------------------------------ */
    /* Browse between different Commands */

    /**
     * Perform all the Hidden Actions of the current Command.
     */
    private void runHiddenActions(State currState) {
        List<Action> hiddenFilteredActions = this.hiddenActions.values().stream().filter(Runner::isActivated).toList();
        if (!hiddenFilteredActions.isEmpty()) {
            this.ui.cli.sepAlt();
            this.ui.cli.centerDisplay("Application Notifications");
            this.ui.cli.sep();
            hiddenFilteredActions.forEach(action -> {
                action.run(currState);
            });
            this.ui.cli.sepAlt();
        }
    }

    /**
     * Check used to accept or refuse a Command in specifics conditions.
     * Can be modified in specifics Commands.
     */
    public Command filterCommand(State currState, Command previousCommand) {
        return this;
    }

    /**
     * Move the user into a specific Command in specifics conditions.
     * Check automatically done after an Action has been performed.
     * Can be modified in specifics Commands.
     */
    public Command filterAction(State currState, Command previousCommand) { return this; }

    /**
     * Run the next Action or check the next Command.
     */
    public Command run(State currState) {
        Runner nextRunner = methods.get(currState.getCMDRunner());
        if (nextRunner == null || !nextRunner.isActivated()) { this.ui.notExist(currState.getCMDRunner()); return this; }

        if (nextRunner instanceof Command nextCommand) {
            feedPreviousCommandInNext(nextCommand);
            return nextCommand.filterCommand(currState, this);

        } else if (nextRunner instanceof Action nextAction){
            nextAction.run(currState);
            return this.filterAction(currState, this);
        }

        throw new IllegalStateException();
    }

    /* ------------------------------------------------------------------ */
    /* Lister & Helper */
    public ArrayList<Pair<String, Runner>> getMethods() {
        ArrayList<Pair<String, Runner>> cmdCollection = new ArrayList<>();
        methods.forEach((cmd, method) -> {
            if (method.isActivated()) {cmdCollection.add(new Pair<>(cmd, method));}
        });
        return cmdCollection;
    }

    public void show(State currState) {
        if (!hiddenActions.isEmpty()) {this.runHiddenActions(currState);}
        this.ui.setup(currState);
    }

    /**
     * Show all the information about all the Sub Commands/Actions
     */
    public void help() {
        ui.displayHelp(this, this.getMethods());
    }
}
