package be.uclouvain.linfo2252.sme10.runners.commands.conversations;

import be.uclouvain.linfo2252.sme10.runners.State;
import be.uclouvain.linfo2252.sme10.runners.actions.conversations.ListConversationAction;
import be.uclouvain.linfo2252.sme10.runners.commands.Command;

public class ConversationCommand extends Command {
    @Override
    public String info() {
        return "See option on the user conversation";
    }

    @Override
    public void feedAll() {
        feedOne("list", new ListConversationAction());
        feedOne("select", new SelectedConversationCommand());
    }

    @Override
    public Command filterCommand(State currState, Command previousCommand) {
        currState.resetCurrConv();
        return this;
    }
}
