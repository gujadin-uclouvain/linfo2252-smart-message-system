package be.uclouvain.linfo2252.sme10.runners.commands.conversations;

import be.uclouvain.linfo2252.sme10.runners.State;
import be.uclouvain.linfo2252.sme10.runners.actions.conversations.ShowHistoryConversationAction;
import be.uclouvain.linfo2252.sme10.runners.actions.conversations.messages.SendEmojiAction;
import be.uclouvain.linfo2252.sme10.runners.actions.conversations.messages.SendMessageAction;
import be.uclouvain.linfo2252.sme10.runners.actions.conversations.messages.SendPictureAction;
import be.uclouvain.linfo2252.sme10.runners.commands.Command;

public class SelectedConversationCommand extends Command {
    @Override
    public String info() {
        return "Select a conversation (thanks to its ID) and makes interactions in it";
    }

    @Override
    public void feedAll() {
        feedOneOption("sendMessage", new SendMessageAction(), "sendWritten", true);
        feedOneOption("sendPicture", new SendPictureAction(), "sendPicture", false);
        feedOneOption("sendEmoji", new SendEmojiAction(), "sendEmoji", true);
        feedOne("history", new ShowHistoryConversationAction());
    }

    private void selectConversation(State currState) {
        String selectedConv = currState.getCMDArgs()[0];
        int convId;
        try {
            convId = Integer.parseInt(selectedConv);
            currState.setCurrConv(
                    currState.getCurrUser().getConversations().get(convId)
            );
        } catch (Exception ignored) {}
    }

    @Override
    public Command filterCommand(State currState, Command previousCommand) {
        currState.resetCurrConv();
        if (this.equalNbrArgs(1, currState.getCMDArgs().length)) return previousCommand;
        this.selectConversation(currState);
        if (currState.getCurrConv() != null) {
            this.ui.success("Conversation successfully set\n");
            return this;
        } else {
            this.ui.fail("No Conversation has been found\n");
            return previousCommand;
        }
    }
}
