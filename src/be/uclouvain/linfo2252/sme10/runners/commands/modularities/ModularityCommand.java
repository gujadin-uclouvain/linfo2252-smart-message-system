package be.uclouvain.linfo2252.sme10.runners.commands.modularities;

import be.uclouvain.linfo2252.sme10.runners.actions.modularities.toggles.ActivateModularityAction;
import be.uclouvain.linfo2252.sme10.runners.actions.modularities.toggles.DeactivateModularityAction;
import be.uclouvain.linfo2252.sme10.runners.actions.modularities.ListModularityAction;
import be.uclouvain.linfo2252.sme10.runners.commands.Command;

public class ModularityCommand extends Command {
    @Override
    public String info() {
        return "Activate / Deactivate application features.";
    }

    @Override
    public void feedAll() {
        feedOne("list", new ListModularityAction());
        feedOne("activate", new ActivateModularityAction());
        feedOne("deactivate", new DeactivateModularityAction());
    }
}
