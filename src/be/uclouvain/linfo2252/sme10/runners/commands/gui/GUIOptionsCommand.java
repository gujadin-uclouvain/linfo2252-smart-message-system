package be.uclouvain.linfo2252.sme10.runners.commands.gui;

import be.uclouvain.linfo2252.sme10.runners.actions.gui.SwitchColorGUIAction;
import be.uclouvain.linfo2252.sme10.runners.commands.Command;

public class GUIOptionsCommand extends Command {
    @Override
    public String info() {
        return "Options applicable only for the GUI";
    }

    @Override
    public void feedAll() {
        this.feedOne("color", new SwitchColorGUIAction());
    }
}
