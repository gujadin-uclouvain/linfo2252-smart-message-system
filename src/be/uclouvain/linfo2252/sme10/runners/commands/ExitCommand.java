package be.uclouvain.linfo2252.sme10.runners.commands;

import be.uclouvain.linfo2252.sme10.runners.Runner;
import be.uclouvain.linfo2252.sme10.runners.State;

public final class ExitCommand extends Command {
    public ExitCommand() {}

    @Override
    public void feedOne(String cmd, Runner runner) {}

    @Override
    public void feedAll() {}

    @Override
    public String info() { return "Exit gracefully the program"; }

    @Override
    public Command run(State currState) {
        return null;
    }
}
