package be.uclouvain.linfo2252.sme10.runners.commands;

import be.uclouvain.linfo2252.sme10.runners.State;
import be.uclouvain.linfo2252.sme10.runners.actions.connections.LoggingAction;
import be.uclouvain.linfo2252.sme10.runners.actions.notifications.DefaultNotificationAction;
import be.uclouvain.linfo2252.sme10.runners.actions.notifications.MuteNotificationAction;
import be.uclouvain.linfo2252.sme10.runners.actions.notifications.VibrateNotificationAction;
import be.uclouvain.linfo2252.sme10.runners.commands.connections.ConnectionCommand;
import be.uclouvain.linfo2252.sme10.runners.commands.contacts.ContactCommand;
import be.uclouvain.linfo2252.sme10.runners.commands.conversations.ConversationCommand;
import be.uclouvain.linfo2252.sme10.runners.commands.gui.GUIOptionsCommand;
import be.uclouvain.linfo2252.sme10.runners.commands.modularities.ModularityCommand;
import be.uclouvain.linfo2252.sme10.runners.commands.profiles.ProfileCommand;

public class MainCommand extends Command {
    private Command connectionCommand;

    private Command getConnectionCommand() {
        if (this.connectionCommand == null) {
            this.connectionCommand = new ConnectionCommand(this);
        } return this.connectionCommand;
    }

    @Override
    public String info() {
        return "Main application commands";
    }

    @Override
    public void feedAll() {
        feedOne("contacts", new ContactCommand());
        feedOne("modularity", new ModularityCommand());
        feedOne("profile", new ProfileCommand());
        feedOne("conversation", new ConversationCommand());
        feedOneOption("GUI", new GUIOptionsCommand(), "GUIOptions", true);
        feedOneOption("disconnect", this.getConnectionCommand(), "login", true);

        feedOneHiddenOption("defaultNotificationMode", new DefaultNotificationAction(), true);
        feedOneHiddenOption("vibrateNotificationMode", new VibrateNotificationAction(), false);
        feedOneHiddenOption("muteNotificationMode", new MuteNotificationAction(), false);
    }

    @Override
    public Command filterAction(State currState, Command previousCommand) {
        return currState.getCurrUser() != null ? super.filterAction(currState, previousCommand) : this.getConnectionCommand();
    }
}
