package be.uclouvain.linfo2252.sme10.runners.modularities;

import be.uclouvain.linfo2252.sme10.runners.Runner;

import javax.management.openmbean.KeyAlreadyExistsException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public enum Modularity {
    INSTANCE;
    private final LinkedHashMap<String, Runner> options = new LinkedHashMap<>();

    private void checkUniqueOptionName(String optionName, Runner runner) {
        if (options.containsKey(optionName)) throw new KeyAlreadyExistsException("The option key '"+optionName+"' already exists for the Runner '"+runner.getClass().getName()+"'. Choose another one.");
    }

    public Set<Map.Entry<String, Runner>> getAll() {
        return options.entrySet();
    }

    public Runner getOne(String key) {
        return options.get(key);
    }

    public void feedOne(String optionName, Runner runner) {
        if (runner == null) throw new NullPointerException("Runner must not be at null");
        checkUniqueOptionName(optionName, runner);
        options.put(optionName, runner);
    }
}
