package be.uclouvain.linfo2252.sme10.runners;

import be.uclouvain.linfo2252.sme10.runners.ui.UI;

public abstract class Runner {
    protected final UI ui;

    private String optionName;
    private boolean state = true;

    public Runner() {
        this.ui = UI.INSTANCE;
    }

    /**
     * Useful if a Runner is not mandatory (optional)
     */
    public void setOptionName(String optionName) { this.optionName = optionName; }

    public boolean isActivated() {return this.state;}
    public void activate() {if (optionName != null) this.state = true;}
    public void deactivate() {if (optionName != null) this.state = false;}


    /**
     * Check if the number of arguments in input is correct
     */
    private boolean checkNbrArgsWithCustomLogic(int nbrArgsNeeded, int nbrArgs, boolean logic) {
        if (logic) {
            this.ui.fail("The number of arguments must be '%d' and not '%d'\n".formatted(nbrArgsNeeded, nbrArgs));
            return true;
        } return false;
    }

    /**
     * Check if the number of arguments in input is equal
     */
    public boolean equalNbrArgs(int nbrArgsNeeded, int nbrArgs) {
        return checkNbrArgsWithCustomLogic(nbrArgsNeeded, nbrArgs, nbrArgsNeeded != nbrArgs);
    }

    /**
     * Check if the number of arguments in input is bigger
     */
    public boolean biggerNbrArgs(int nbrArgsNeeded, int nbrArgs) {
        return checkNbrArgsWithCustomLogic(nbrArgsNeeded, nbrArgs, nbrArgsNeeded > nbrArgs);
    }

    /**
     * Information about the current Command/Action
     */
    public abstract String info();
}
