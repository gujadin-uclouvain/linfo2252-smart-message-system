package be.uclouvain.linfo2252.sme10.messages;

import be.uclouvain.linfo2252.sme10.users.User;

import java.util.Date;

public class MessageWritten implements Message {
    private String content;
    private final Date sendingDate;
    private User sender;

    /* ------------------------------------------------------------------ */
    /* Constructors */

    public MessageWritten(String content) {
        setContent(content);
        this.sendingDate = new Date();
    }
    public MessageWritten(String[] content) {
        setContent(content);
        this.sendingDate = new Date();
    }

    /* ------------------------------------------------------------------ */
    /* Dates Management */

    public Date getSendingDate() { return sendingDate; }

    /* ------------------------------------------------------------------ */
    /* Message Content */

    public String getContent() { return content; }

    public void setContent(String content) {
        this.content = content;
    }
    public void setContent(String[] content) {
        StringBuilder finalContent = new StringBuilder("");
        for (String word : content) {
            finalContent.append(word).append(" ");
        }
        finalContent.delete(finalContent.length()-1, finalContent.length());
        this.content = finalContent.toString();
    }

    /* ------------------------------------------------------------------ */
    /* Message Sender */

    public User getSender() {return sender;}

    public void setSender(User sender) {
        if (getSender() == null) this.sender = sender;
    }

    /* ------------------------------------------------------------------ */
    /* New Println */

    @Override
    public String toString() {
        return "["+getSendingDate()+"] " + getSender().getUsername()+": " + getContent();
    }
}
