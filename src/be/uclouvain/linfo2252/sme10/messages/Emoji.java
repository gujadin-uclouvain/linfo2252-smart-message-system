package be.uclouvain.linfo2252.sme10.messages;

public enum Emoji {
    smile("\uD83D\uDE42"),
    sad("\uD83E\uDD72"),
    lol("\uD83E\uDD23"),
    zip("\uD83E\uDD10"),
    beg("\uD83E\uDD7A"),
    thumbsup("\uD83D\uDC4D"),
    okhand("\uD83D\uDC4C");

    private final String emojiCode;
    Emoji(String emojiCode) { this.emojiCode = emojiCode; }
    public String get() { return this.emojiCode; }

    @Override
    public String toString() {
        return "%s %s".formatted(this.emojiCode, this.name());
    }
}
