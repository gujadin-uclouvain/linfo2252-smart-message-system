package be.uclouvain.linfo2252.sme10.messages;

public class MessagePicture extends MessageWritten {
    boolean originQuality;

    /* ------------------------------------------------------------------ */
    /* Constructors */

    public MessagePicture(String content) {
        super(content);
        setGoodQuality(true);
    }

    /* ------------------------------------------------------------------ */
    /* Quality */

    /** Set the quality of the sending Message **/
    public void setGoodQuality(boolean isGoodQuality) {
        this.originQuality = isGoodQuality;
    }
}
