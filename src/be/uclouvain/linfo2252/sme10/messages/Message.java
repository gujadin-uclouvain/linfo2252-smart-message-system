package be.uclouvain.linfo2252.sme10.messages;

import be.uclouvain.linfo2252.sme10.users.User;

import java.util.Date;

public interface Message {
    /* ------------------------------------------------------------------ */
    /* Dates Management */

    /** Get when the Message was sent **/
    Date getSendingDate();

    /* ------------------------------------------------------------------ */
    /* Message Content */

    /** Get the content of the Message **/
    String getContent();

    /** Set the content of the Message **/
    void setContent(String content);

    /* ------------------------------------------------------------------ */
    /* Message Sender */

    /** Get the sender **/
    User getSender();

    /** Set the sender **/
    void setSender(User sender);
}
