package be.uclouvain.linfo2252.sme10;

import be.uclouvain.linfo2252.sme10.runners.commands.Command;
import be.uclouvain.linfo2252.sme10.runners.commands.MainCommand;
import be.uclouvain.linfo2252.sme10.runners.databases.Populator;
import be.uclouvain.linfo2252.sme10.runners.State;
import be.uclouvain.linfo2252.sme10.runners.commands.ExitCommand;

public class Main {

    public static void main(String[] args) {
        State currState = new State();
        Command mainCommand = new MainCommand();
        currState.setCurrCommand(mainCommand.filterAction(currState, mainCommand));

        Populator.populateUsersDB();

        while (!(currState.getCurrCommand() instanceof ExitCommand)) {
            currState.getCurrCommand().show(currState);
        }
        System.exit(0);
    }
}
