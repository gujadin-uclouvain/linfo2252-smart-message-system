package be.uclouvain.linfo2252.sme10.users;

public class SecureUser extends SimpleUser {
    private String password;

    /* ------------------------------------------------------------------ */
    /* Constructors */

    public SecureUser(String idName, String passwordInput) {
        super(idName);
        this.password = passwordInput;
    }

    /* ------------------------------------------------------------------ */
    /* Password Management */

    /** Modify an existing password **/
    void changePassword(String oldPassword, String newPassword) {
        if (isCorrectPassword(oldPassword) && !oldPassword.equals(newPassword)) {
            setPassword(newPassword);
        }
    }

    /** Check if the password in input is the same as registered **/
    public boolean isCorrectPassword(String passwordInput) {return this.password.equals(passwordInput);}

    /** Set a new password **/
    private void setPassword(String password) {this.password = password;}

    /** Get the existing password **/
    private String getPassword() {return password;}
}
