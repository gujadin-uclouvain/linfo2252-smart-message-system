package be.uclouvain.linfo2252.sme10.users;

import be.uclouvain.linfo2252.sme10.conversations.Conversation;
import be.uclouvain.linfo2252.sme10.conversations.Dialogue;
import be.uclouvain.linfo2252.sme10.messages.Message;

import java.util.HashMap;
import java.util.LinkedList;

public class SimpleUser implements User {
    private final int id;

    private String username;

    private String firstName;
    private String lastName;

    private boolean hasNotifications = false;

    private final HashMap<Integer, User> contacts;
    private LinkedList<Conversation> conversations;

    /* ------------------------------------------------------------------ */
    /* Constructors */

    public SimpleUser(String username) {
        this.id = GenUserID.getInstance().getId();
        setUsername(username);
        this.contacts = new HashMap<>();
        this.conversations = new LinkedList<>();
    }

    /* ------------------------------------------------------------------ */
    /* ID */

    public int getId() {return id;}

    /* ------------------------------------------------------------------ */
    /* Username */

    public String getUsername() {return this.username;}

    public void setUsername(String username) {this.username = username;}

    /* ------------------------------------------------------------------ */
    /* First and Last names */

    public String getFirstName() {return firstName;}

    public String getLastName() {return lastName;}

    public void setNames(String firstName, String lastName) {
        this.firstName = firstName; this.lastName = lastName;
    }

    /* ------------------------------------------------------------------ */
    /* Contact Management */

    public HashMap<Integer, User> getContacts() {return contacts;}

    public boolean addContact(User newFriend) {
        if (!this.equals(newFriend) && getContacts().putIfAbsent(newFriend.getId(), newFriend) == null) {
            if (!dialogueExists(newFriend)) {
                new Dialogue(this, newFriend);
            } return true;
        } return false;
    }

    public User removeContact(User oldFriend) {
        return getContacts().remove(oldFriend.getId());
    }

    /* ------------------------------------------------------------------ */
    /* Conversation Management */

    public LinkedList<Conversation> getConversations() {return this.conversations;}

    public boolean dialogueExists(User user) {
        for (Conversation conv: this.getConversations()) {
            if (conv instanceof Dialogue dialogue) {
                if (dialogue.getMembers().containsKey(user.getId())) {
                    return true;
                }
            }
        }
        return false;
    }

    public void addConversation(Conversation newConv) {getConversations().addFirst(newConv);}

    public boolean removeConversation(Conversation oldConv) {return getConversations().remove(oldConv);}

    /* ------------------------------------------------------------------ */
    /* Notification Management */

    public boolean hasNotification() { return this.hasNotifications; }

    public void sendNotification() { this.hasNotifications = true; }

    public void readNotification() { this.hasNotifications = false; }


    /* ------------------------------------------------------------------ */
    /* Interactions in Conversations */

    public void send(Message msg, Conversation conv) {
        msg.setSender(this);
        conv.send(msg);
    }

    /* ------------------------------------------------------------------ */
    /* New Println */

    @Override
    public String toString() {
        return getUsername()+"#"+getId();
    }
}