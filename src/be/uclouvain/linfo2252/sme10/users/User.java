package be.uclouvain.linfo2252.sme10.users;

import be.uclouvain.linfo2252.sme10.conversations.Conversation;
import be.uclouvain.linfo2252.sme10.messages.Message;

import java.util.HashMap;
import java.util.LinkedList;

public interface User {
    /* ------------------------------------------------------------------ */
    /* ID */

    /** Getter for the ID **/
    int getId();

    /* ------------------------------------------------------------------ */
    /* Username */

    /** Getter for the username **/
    String getUsername();

    /** Setter for the username **/
    void setUsername(String username);

    /* ------------------------------------------------------------------ */
    /* First and Last names */

    /** Get First name **/
    String getFirstName();

    /** Get First name **/
    String getLastName();

    /** Set First and Last names **/
    void setNames(String firstName, String lastName);

    /* ------------------------------------------------------------------ */
    /* Contact Management */

    /** Getter for the contact management **/
    HashMap<Integer, User> getContacts();

    /** Put a Contact in the Current User's Contact Book **/
    boolean addContact(User newFriend);

    /** Remove a Contact from the Current User's Contact Book **/
    User removeContact(User oldFriend);

    /* ------------------------------------------------------------------ */
    /* Conversation Management */

    /** Getter for the Conversation management **/
    LinkedList<Conversation> getConversations();

    /** Add a Conversation in the Current User's list (First) **/
    void addConversation(Conversation newConv);

    /** Remove a Conversation from the Current User's list **/
    boolean removeConversation(Conversation oldConv);

    /* ------------------------------------------------------------------ */
    /* Notification Management */

    /** Check if the user has a Notification signal saved **/
    boolean hasNotification();

    /** Send a Notification signal to this user **/
    void sendNotification();

    /** Read the Notification sent to this user (and so disable the notification signal) **/
    void readNotification();

    /* ------------------------------------------------------------------ */
    /* Interactions in Conversations */

    /** Send a Message in a Conversation **/
    void send(Message msg, Conversation conv);
}
