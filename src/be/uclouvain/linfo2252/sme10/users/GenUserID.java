package be.uclouvain.linfo2252.sme10.users;

/**
 * Implement a Singleton
 * Used to Generate an ID for any new Users
 * Useful until there is no User Database
 */
class GenUserID {
    private int id;
    private static GenUserID uniqueGenerator;

    private GenUserID() {
        this.id = 1;
    }

    public int getId() {return id;}
    private void incrementId() {this.id += 1;}

    public static GenUserID getInstance() {
        if (uniqueGenerator == null) {
            uniqueGenerator = new GenUserID();
        } else uniqueGenerator.incrementId();
        return uniqueGenerator;
    }
}
