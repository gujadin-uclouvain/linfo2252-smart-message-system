package be.uclouvain.linfo2252.sme10.conversations;

import be.uclouvain.linfo2252.sme10.messages.Message;
import be.uclouvain.linfo2252.sme10.users.User;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

public class Dialogue implements Conversation {
    private LinkedList<Message> history;
    private HashMap<Integer, User> groupMembers;

    /* ------------------------------------------------------------------ */
    /* Constructors */

    public Dialogue(User user1, User user2) {
        history = new LinkedList<>();
        groupMembers = new HashMap<>();
        addMember(user1); addMember(user2);
        user1.addConversation(this); user2.addConversation(this);
    }

    /* ------------------------------------------------------------------ */
    /* Message history Management */

    public Iterator<Message> iterateAllMessages() {
        return history.iterator();
    }

    public boolean isEmpty() { return history.size() == 0; }

    public void addMessage(Message msg) {
        history.add(msg);
    }

    /* ------------------------------------------------------------------ */
    /* Members Management */

    public HashMap<Integer, User> getMembers() {return groupMembers;}

    public void addMember(User member) {groupMembers.put(member.getId(), member);}

    /* ------------------------------------------------------------------ */
    /* Notifications Management */

    private void sendNotification(User sender) {
        this.groupMembers.forEach((id, user) -> {
            if (!user.equals(sender)) {
                user.sendNotification();
            }
        });
    }

    /* ------------------------------------------------------------------ */
    /* Interactions With Messages */

    public void send(Message msg) {
        history.addLast(msg);
        this.sendNotification(msg.getSender());
    }

    // TODO: Change when implement notification (Maybe use Observers ??)
    public Message receive() {
        return history.getLast();
    }
}
