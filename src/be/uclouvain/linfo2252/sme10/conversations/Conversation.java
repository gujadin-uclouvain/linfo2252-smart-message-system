package be.uclouvain.linfo2252.sme10.conversations;

import be.uclouvain.linfo2252.sme10.messages.Message;
import be.uclouvain.linfo2252.sme10.users.User;

import java.util.HashMap;
import java.util.Iterator;

public interface Conversation {
    /* ------------------------------------------------------------------ */
    /* Message history Management */

    /** Iterator of all the Messages in a Conversation **/
    Iterator<Message> iterateAllMessages();

    boolean isEmpty();

    /** Add a Message in a Conversation **/
    void addMessage(Message msg);

    /* ------------------------------------------------------------------ */
    /* Members Management */

    /** Get all the Members(Users) in a Conversation **/
    HashMap<Integer, User> getMembers();

    /** Add a Member(User) in a Conversation **/
    void addMember(User member);

    /* ------------------------------------------------------------------ */
    /* Interactions With Messages */

    /** Send a Message in a Conversation **/
    void send(Message msg);

    /** Receive a Message in a Conversation **/
    Message receive();
}
