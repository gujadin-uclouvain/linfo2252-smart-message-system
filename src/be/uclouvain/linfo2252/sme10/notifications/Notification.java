package be.uclouvain.linfo2252.sme10.notifications;

public enum Notification {
    DEFAULT("DRIIING !!!"),
    VIBRATE("BZZ-BZZ"),
    MUTE("...");

    private final String notifiedSimulation;
    Notification(String simulation) { this.notifiedSimulation = simulation; }

    public String getNotifiedSimulation() { return this.notifiedSimulation; }
}
