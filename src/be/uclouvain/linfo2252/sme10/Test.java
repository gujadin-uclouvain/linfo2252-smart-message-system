package be.uclouvain.linfo2252.sme10;

import be.uclouvain.linfo2252.sme10.runners.State;
import be.uclouvain.linfo2252.sme10.runners.commands.Command;
import be.uclouvain.linfo2252.sme10.runners.commands.ExitCommand;
import be.uclouvain.linfo2252.sme10.runners.commands.MainCommand;
import be.uclouvain.linfo2252.sme10.runners.databases.Populator;
import be.uclouvain.linfo2252.sme10.runners.ui.cli.Input;

import java.util.concurrent.TimeUnit;

public class Test {
    public static void main(String[] args) throws InterruptedException {
        State currState = new State();
        Command mainCommand = new MainCommand();
        currState.setCurrCommand(mainCommand.filterAction(currState, mainCommand));

        Populator.populateUsersDB();

        String[] setCommands = new String[] {
                "login 1",
                "contacts",
                "list",
                "add 3",
                "add 4",
                "list",
                "del 3",
                "previous",
                "exit"
        };
        Input.setInput(setCommands);

        int i = 0;
        while (!(currState.getCurrCommand() instanceof ExitCommand)) {
            TimeUnit.SECONDS.sleep(3);
            currState.getCurrCommand().show(currState);
            i++;
        }
        System.exit(0);
    }
}
